package mailService;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class MailService {
    final String username;
    final String password;
    final Properties props;

    private PrintWriter writer;
    /**
     * Builds a mail service class, used for sending e-mails.
     * The credentials provided should be the ones needed to
     * autenthicate to the SMTP server (GMail by default).
     *
     * @param username username to log in to the smtp server
     * @param password password to log in to the smtp server
     */
    public MailService(String username, String password) {
        this.username = username;
        this.password = password;

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
    }


    /**
     * Sends an email with the subject and content specified, to
     * the address specified.
     *
     * @param to address to send email to
     * @param subject subject of the email
     * @param content content of the email
     */
    public void sendMail(String to, String subject, String content) {
        Session session = (Session) Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);

            saveContent(content);

            System.out.println("Mail sent.");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void saveContent(String content) {
        String path = "E:/ds.handson.assignment3/";
        String title = new SimpleDateFormat("dd.MM.yyyy - hh_mm_ss").format(Calendar.getInstance().getTime());

        FileWriter fileWriter;
        BufferedWriter bufferedWriter;

        try {
            fileWriter = new FileWriter(path + title + ".txt", true);
            bufferedWriter = new BufferedWriter(fileWriter);
            writer = new PrintWriter(bufferedWriter);

            writer.println(content);
            writer.println("\n");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }

}